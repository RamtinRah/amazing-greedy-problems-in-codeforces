/// link to problem : https://codeforces.com/problemset/problem/665/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e6 + 3;
const int maxlog = 20;
const int mod = 1e9 + 7;
const int maxh = 2 * 350;
bool mark[maxn];
int a[maxn];
int main()
{
	ios_base::sync_with_stdio(false) , cin.tie(0) , cout.tie(0); cout.precision(20);
	for(ll i = 2 ; i < maxn ; i ++ ){
        if(!mark[i]){
            for(ll j = i * i ; j < maxn ; j += i)
                mark[j] = true;
        }
	}
	int n ;
	cin >> n;
	int cnt = 0;
	for(int i = 1 ; i <= n ; i ++ ){
        cin >> a[i];
        if(a[i] == 1)
            cnt ++ ;
	}
    sort(a + 1 , a + n + 1);
    if(cnt){
        for(int j = n ; j > 1 ; j -- ){
            if(a[j] != 1 && !mark[a[j] + 1]){
                cout << cnt + 1 << endl;
                for(int k = 0 ; k < cnt ; k ++ )
                    cout << 1 << " ";
                cout << a[j];
                return 0;
            }
            else if(!mark[a[j] + 1]){
                cout << cnt << endl;
                for(int k = 0 ; k < cnt ; k ++ )
                    cout << 1 << " ";
                return 0;
            }
        }
    }
    for(int i = 1 ; i <= n ; i ++ ){
        for(int j = i + 1 ; j <= n ; j ++ ){
            if(!mark[a[i] + a[j]]){
                cout << 2 << endl << a[i] << " " << a[j];
                return 0;
            }
        }
    }
    cout << 1 << endl << a[1];
	return 0;
}
