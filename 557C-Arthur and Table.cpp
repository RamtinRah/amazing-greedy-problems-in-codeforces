///link to problem : https://codeforces.com/problemset/problem/557/C


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int,int>::iterator setit;
const int maxn=1e5+6;
const int maxm=200+6;
const int MOD=1e9+7;
const double eps=1e-9;
pii a[maxn];
int suf[maxn];
int cnt[maxm];
int ans=1e18;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n; cin>>n;
    for(int i=1;i<=n;i++)
        cin>>a[i].first;
    for(int i=1;i<=n;i++)
        cin>>a[i].second;
    sort(a+1,a+n+1);
    for(int i=n;i>=1;i--)
        suf[i]=suf[i+1]+a[i].second;
    int num=0;
    int sum=0;
    vector<int> ToAdd;
    for(int i=1;i<=n;i++)
    {
        int ans1=0;
        if(a[i].first==a[i-1].first)
            num++;
        else
        {
            num=1;
            for(int j=0;j<ToAdd.size();j++)
            {
                sum+=ToAdd[j];
                cnt[ToAdd[j]]++;
            }
            ToAdd.clear();
        }
        int need=num-1;
        for(int j=maxm-1;j>0;j--)
        {
            if(cnt[j]>=need)
            {
                ans1-=need*j;
                break;
            }
            else
            {
                ans1-=cnt[j]*j;
                need-=cnt[j];
            }
        }
        ans1=ans1+sum+suf[i+1];
        ans=min(ans1,ans);
        ToAdd.pb(a[i].second);
    }
    cout<<ans;
    return 0;
}
