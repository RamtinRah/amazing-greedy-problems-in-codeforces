///link to problem : https://codeforces.com/problemset/problem/115/B


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 200;
const int maxlog = 64;
const int mod = 1e9 + 7;
const int maxh = 2 * 350;
const ll inf = 9e18 + 4;
string s[maxn];
int l[maxn] , r[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(30);
    int n , m;
    cin >> n >> m;
    for(int i = 0 ; i < n ; i ++ ){
        cin >> s[i];
        l[i] = -1 , r[i] = -1;
        for(int j = 0 ; j < m ; j ++ ){
            if(s[i][j] == 'W'){
                l[i] = j;
                break;
            }
        }
        for(int j = m - 1 ; j >= 0 ; j -- ){
            if(s[i][j] == 'W'){
                r[i] = j;
                break;
            }
        }
    }
    while(l[n - 1] == -1)
        n -- ;
    if(!n){
        cout << 0;
        return 0;
    }
    if(n == 1){
        cout << r[0];
        return 0;
    }
    int cur = 0 , ans = 0;
    for(int i = 0 ; i < n - 1; i ++ ){
        if(i % 2 == 0 && r[i] != -1){
            ans += r[i] - cur;
            cur = r[i];
        }
        else if(i % 2 == 1 && l[i] != -1){
            ans += cur - l[i];
            cur = l[i];
        }
        if(i % 2 == 0){
            if(r[i + 1] == -1){
                ans ++ ;
            }
            else{
                ans += max(cur , r[i + 1]) - cur;
                cur = max(cur , r[i + 1]);
                ans ++ ;
            }
        }
        else{
            if(l[i + 1] == -1)
                ans ++ ;
            else{
                ans += cur - min(cur , l[i + 1]);
                cur = min(cur , l[i + 1]);
                ans ++ ;
            }
        }
    }
    int i = n - 1;
    if(i % 2 == 0 && r[i] != -1){
        ans += r[i] - cur;
        cur = r[i];
    }
    else if(i % 2 == 1 && l[i] != -1){
        ans += cur - l[i];
        cur = l[i];
    }
    cout << ans;
    return 0;
}
