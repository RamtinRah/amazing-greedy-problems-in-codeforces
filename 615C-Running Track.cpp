///link to problem : https://codeforces.com/problemset/problem/615/C


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 5;
const int maxlog = 22;
const int mod = 1e9 + 7;
const int sq = 350;
vector<pii> ans;
string s , t;
int arr1[maxn] , arr2[maxn];
bool mark[maxn];
void solve(int pl){
    for(int i=0;i<s.size();i++){
        arr1[i] = 0;
        arr2[i] = 0;
    }
    int res = 0;
    for(int i=0;i<s.size();i++){
        int k2 = i;
        int k = pl;
        while(true){
            if(k == t.size() || k2 == s.size() || t[k] != s[k2])
                break;
            arr1[i] ++ ;
            mark[k] = true;
            k2 ++ ;
            k ++ ;
        }
        res = max(res , arr1[i]);
    }
    for(int i=s.size()-1;i>=0;i--){
        int k2 = i;
        int k = pl;
        while(true){
            if(k == t.size() || k2 < 0 || t[k] != s[k2])
                break;
            arr2[i] ++ ;
            mark[k] = true;
            k2 -- ;
            k ++ ;
        }
        res = max(res , arr2[i]);
    }
    if(res == 0){
        cout << -1;
        exit(0);
    }
    for(int i=0;i<s.size();i++){
        if(arr1[i] == res){
            ans.pb(mp(i + 1 , i + arr1[i]));
            break;
        }
        if(arr2[i] == res){
            ans.pb(mp(i + 1 , i - arr2[i] + 2));
            break;
        }
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> s >> t;
    for(int i=0;i<t.size();i++){
        if(!mark[i])
            solve(i);
    }
    cout << ans.size() << endl;
    for(int i=0;i<ans.size();i++){
        cout << ans[i].first << " " << ans[i].second << endl;
    }
    return 0;
}
