///link to problem : https://codeforces.com/problemset/problem/125/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 3e4 + 4;
const int maxlog = 64;
const int mod = 1e9 + 7;
const int maxh = 2 * 350;
const int inf = 2e7 + 2;
int n;
int a[maxn];
vector<int> v1 , v2;
int tof;
bool tof2;
void solve(){
    int id = v1.size() + v2.size();
    if(tof >= inf){
        if(! tof2){
            v1.clear();
            v2.clear();
            reverse(a , a + n);
            tof2 = true;
            tof = 0;
            solve();
        }
        else{
            cout << "No solution";
            exit(0);
        }
    }
    if(id == n){
        if(tof2){
            reverse(v1.begin() , v1.end());
            reverse(v2.begin() , v2.end());
        }
        if(v1.size() == 0){
            v1.pb(v2.back());
            v2.pop_back();
        }
        if(v2.size() == 0){
            v2.pb(v1.back());
            v1.pop_back();
        }
        for(int i = 0 ; i < v1.size() ; i ++ )
            cout << v1[i] << " ";
        cout << endl;
        for(int i = 0 ; i < v2.size() ; i ++ )
            cout << v2[i] << " ";
        exit(0);
    }
    else{
        if(v1.size() < 2 || (a[id] - v1.back() == v1[1] - v1[0])){
            v1.pb(a[id]);
            tof ++ ;
            solve();
            v1.pop_back();
        }
        if(v2.size() < 2 || (a[id] - v2.back() == v2[1] - v2[0])){
            v2.pb(a[id]);
            tof ++ ;
            solve();
            v2.pop_back();
        }
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(30);
    cin >> n;
    for(int i = 0 ; i < n; i ++ )
        cin >> a[i];
    solve();
    cout << "No solution";
    return 0;
}
