///link to problem : https://codeforces.com/problemset/problem/353/B


#include <bits/stdc++.h>
using namespace std;
#define pb push_back
const int maxn=2e2+6;
vector<int> cnt[maxn],res;
int ans[maxn],d[2];
bool b;
int main()
{
    int n; cin>>n;
    for(int i=0;i<2*n;i++)
    {
        int x; cin>>x;
        cnt[x].pb(i);
    }
    for(int i=10;i<100;i++)
    {
        for(int j=0;j<cnt[i].size();j++)
        {
            if(j==0 || j==1)
            {
                ans[cnt[i][j]]=b;
                d[b]++;
                b^=1;
            }
            else res.pb(cnt[i][j]);
        }
    }
    for(int i=0;i<res.size();i++)
    {
        ans[res[i]]=b;
        b^=1;
    }
    cout<<d[0] * d[1]<<endl;
    for(int i=0;i<2*n;i++)
        cout<<ans[i]+1<<" ";
    return 0;
}
