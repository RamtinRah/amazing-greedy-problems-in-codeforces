///link to problem : https://codeforces.com/problemset/problem/476/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 5e3 + 4;
const int maxlog = 22;
const int mod = 1e9 + 7;
const int sq = 350;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    int n , k;
    cin >> n >> k;
    cout << k * (6 * n - 1) << endl;
    for(int i = 0 ; i < n ; i ++)
        cout << k * (6 * i + 1) << " " << k * (6 * i + 2) << " " << k * (6 * i + 3) << " " << k * (6 * i + 5) << endl;
    return 0;
}
