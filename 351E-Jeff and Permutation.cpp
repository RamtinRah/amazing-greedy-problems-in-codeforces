///link to problem : https://codeforces.com/problemset/problem/351/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 2000 + 5;
const int maxk = 5e6 + 3;
const int maxlog = 60;
const ll mod = 1e18 + 7;
const int sq = 1000 ;
const int inf = 1e9 + 43;
const ld pi = 3.1415926535897932384626433832795028841971693993751;
int a[maxn];
int b[maxn]; /// rasti haye koochiktar
int c[maxn] ; /// chapi haye koochiktar
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20) ;
    int n;
    cin >> n;
    for(int i = 0 ; i < n ; i ++ ){
        cin >> a[i];
        a[i] = abs(a[i]);
    }
    int ans = 0;
    for(int i = 0 ; i < n ; i ++ ){
        for(int j = i + 1 ; j < n ; j ++ )
            if(a[j] < a[i]){
                b[i] ++ ;
                ans ++ ;
            }
    }
    for(int i = 0 ; i < n ; i ++ ){
        for(int j = 0 ; j < i ; j ++ )
            if(a[j] < a[i])
                c[i] ++ ;
    }
    int cur = ans ;
    for(int i = 0 ; i < n ; i ++ ){
        ans = min(ans , ans - b[i] + c[i]);
    }
    cout << ans;
    return 0;
}
