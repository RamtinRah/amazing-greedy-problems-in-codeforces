///link to problem : https://codeforces.com/problemset/problem/527/D


#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define mp make_pair
typedef long long ll;
typedef pair<int,int> pii;
const ll MOD=1e9+7;
const int maxn=2e5+4;
const int INF=1e9;
pii a[maxn];
bool cmp(pii a,pii b)
{
    return a.first+a.second<=b.first+b.second;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n; cin>>n;
    for(int i=0;i<n;i++)
        cin>>a[i].first>>a[i].second;
    sort(a,a+n,cmp);
    int ans=1, lst=0;
    for(int i=1;i<n;i++)
    {
        if(a[i].first-a[i].second>=a[lst].first+a[lst].second)
        {
            lst=i;
            ans++;
        }
    }
    cout<<ans;
    return 0;
}
