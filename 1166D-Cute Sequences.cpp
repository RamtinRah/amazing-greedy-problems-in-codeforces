///link to problem : https://codeforces.com/problemset/problem/1166/D

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 4e6 + 43;
const int maxm = 2e3 + 43;

const int maxlog = 22;
const ll mod = 2e9 + 1;
const int sq = 340;
const int inf = 2e9 + 34;
const ld PI  =3.141592653589793238463;
ll p[maxn];
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    p[0] = 1;
    for(int i = 1 ; i <= 50 ; i ++ )
        p[i] = p[i - 1] * (ll)2;

    int q;
    cin >> q;
    while(q -- ){
        ll a , b , m;
        cin >> a >> b >> m;
        if(a == b){
            cout << 1 << " " << a << endl;
            continue;
        }
        vector<ll> v;
        bool done = false;
        for(ll n = 2 ; n <= 50 ; n ++ ){
            v.clear();
            ll x = b - p[n - 2] * (a + 1);
            if(x < 0)
                break;
            for(int i = 1 ; i < n - 1 ; i ++ ){
                ll cur = x / p[n - i - 2];
                cur = min(cur , m - 1);
                v.pb(cur + 1);
                x -= cur * p[n - i - 2];
            }
            if(x >= 0 && x < m){
                done = true;
                v.pb(x + 1);
                cout << n << " " << a << " ";

                ll sum = a;
                for(int i = 2 ; i <= n ; i ++ ){
                    cout << sum + v[i - 2] << " ";
                    sum += sum + v[i - 2];
                }
                cout << endl;
                break;
            }
        }
        if(!done){
            cout << -1 << endl;
        }
    }
    return 0;
}
