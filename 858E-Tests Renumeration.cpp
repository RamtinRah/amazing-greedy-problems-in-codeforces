
///link to problem : https://codeforces.com/contest/858/problem/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 2e5 + 4;
const int maxlog = 31;
const ll mod = 1e9 + 7;
const int sq = 1000 ;
const int inf = 1e9 + 43;
const ld pi = 3.1415926535897932384626433832795028841971693993751;
vector<pair<int,string> > v;
vector<pii> vec;
int n;
int f(string s){
    int num = 0;
    for(int i = 0 ; i < s.size() ; i ++ ){
        num *= 10;
        if(s[i] >= '0' && s[i] <= '9')
            num += s[i] - '0';
        else
            return -1;
    }
    if(num != 0 && num <= n && s[0] != '0')
        return num;
    else
        return -1;
}
vector<pii> ans ;
vector<pair<string , int> > ans2;
vector<int> wrong1 , wrong2;
int res[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> n;
    int cnt = 0 , st = 0 , cntt = 0;
    for(int i = 0 ; i < n ; i ++ ){
        string s ;
        int type ;
        cin >> s >> type ;
        type ^= 1;
        int x = f(s);
        if(x == -1)
            v.pb(mp(type , s));
        else{
            res[x] = type + 1;
        }
        if(type == 0)
            cnt ++;
    }
    int cur = 0;
    set<int> freepl;
    freepl.insert(0) , freepl.insert(n + 1);
    for(int i = 1 ; i <= n ; i ++ ){
        if(!res[i])
            freepl.insert(i);
        else if(res[i] == 1){
            if(i > cnt)
                wrong2.pb(i);
        }
        else if(res[i] == 2)
            if(i <= cnt)
                wrong1.pb(i);
    }
    while(wrong1.size() || wrong2.size()){
        bool tof = false;
        if(wrong1.size()){
            int x = wrong1.back();
            int k = *freepl.lower_bound(cnt + 1);
            if(k != n + 1){
                freepl.erase(k);
                freepl.insert(x);
                wrong1.pop_back();
                ans.pb(mp(x , k));
                tof = true ;
                res[x] = 0;
                res[k] = 2;
            }
        }
        if(wrong2.size()){
            int x = wrong2.back();
            int k = *freepl.lower_bound(1);
            if(k <= cnt){
                freepl.erase(k);
                freepl.insert(x);
                wrong2.pop_back();
                ans.pb(mp(x , k));
                tof = true ;
                res[x] = 0;
                res[k] = 1;
            }
        }
        if(! tof){
            int x = wrong1.back();
            ans.pb(mp(x , n + 1));
            freepl.insert(x);
            wrong1.pop_back();
            res[n + 1] = 2;
            res[x] = 0;
        }
    }
    if(res[n + 1]){
        int x = *freepl.lower_bound(1);
        res[x] = 2;
        res[n + 1] = 0;
        ans.pb(mp(n + 1 , x));
    }
    sort(v.begin() , v.end());
    cur = 1;
    for(int i = 0 ; i < v.size() ; i ++ ){
        string s = v[i].second ;
        while(res[cur])
            cur ++ ;
        res[cur] = 1;
        ans2.pb(mp(s , cur));
    }
    cout << (int) (ans.size() + ans2.size()) << endl;
    for(int i = 0 ; i < ans.size() ; i ++ )
        cout << "move " << ans[i].first << " " << ans[i].second << endl;
    for(int i = 0 ; i < ans2.size() ; i ++ )
        cout << "move " << ans2[i].first << " " << ans2[i].second << endl;
    return 0;
}
