///link to problem : https://codeforces.com/problemset/problem/521/D
///a very hard and interesting problem





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int,int>::iterator setit;
const int maxn=1e5+4;
const double pi = 3.1415926536;
int skill[maxn];
vector<pii> querry[maxn];
pii ass[maxn];
int improve[maxn];
vector<pair<ld,int> > v;
vector<pii> ans;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int k,n,m; cin>>k>>n>>m;
    for(int i=1;i<=k;i++)
        cin>>skill[i];
    for(int i=1;i<=n;i++)
    {
        int type,a,b; cin>>type>>a>>b;
        improve[i] = type;
        if(type == 1)
        {
            if(b>ass[a].first)
            {
                ass[a].first = b;
                ass[a].second = i;
            }
        }
        else if(type == 2)
            querry[a].pb(mp(b,i));
        else
            v.pb(mp(b,i));
    }
    for(int i=1;i<=k;i++)
    {
        if(ass[i].first>skill[i])
            querry[i].pb(mp(ass[i].first-skill[i],ass[i].second));
        sort(querry[i].begin(),querry[i].end());
        reverse(querry[i].begin(),querry[i].end());
        ll sum = skill[i];
        for(int j=0;j<querry[i].size();j++)
        {
            ld d = ((ld)sum + (ld)querry[i][j].first) / (ld) sum;
            v.pb(mp(d,querry[i][j].second));
            sum += querry[i][j].first;
        }
    }
    sort(v.begin(),v.end());
    reverse(v.begin(),v.end());
    for(int i=0;i<min((int)v.size(),m);i++)
        ans.pb(mp(improve[v[i].second],v[i].second));
    sort(ans.begin(),ans.end());
    cout<< ans.size()<<endl;
    for(int i=0;i<ans.size();i++)
        cout<< ans[i].second << " ";
    return 0;
}
