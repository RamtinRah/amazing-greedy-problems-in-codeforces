///link to problem : https://codeforces.com/contest/1276/problem/C


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 4e5 + 43;
const int maxm = 2e3 + 43;

const int maxlog = 22;
const ll mod = (ll)1e9 + 7;
const int sq = 340;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;

map<int,int> cnt;
set<pii> st;
int d[maxn];
map<pii,int> res;
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    int n ;
    cin >> n;
    for(int i = 0 ; i < n ; i ++ ){
        int x;
        cin >> x;
        st.erase(mp(-cnt[x] , x));
        cnt[x] ++ ;
        st.insert(mp(-cnt[x] , x));
    }
    for(int i = 1 ; i < maxn ; i ++ )
        d[i] = 1;
    bool tof = false;
    for(int i = 2 ; i < maxn ; i ++ ){
        if(tof)
            continue;

        if(i * i >= maxn)
            tof = true;



        for(int j = i * i ; j < maxn ; j += i ){
            d[j] = i;
        }
    }
    for(int ans = n ; ans >= 1 ; ans -- ){
        int a = d[ans];
        int b = ans / d[ans];
        pii maxi = *st.begin();
        if(-maxi.first > a){
            st.erase(st.begin());
            maxi.first ++ ;
            st.insert(maxi);
            continue ;
        }
        int s = 0;
        int cur_x = 0 , cur_y = 0;
        for(set<pii> :: iterator it = st.begin() ; it != st.end() ; it ++ ){
            int c = -1 * (*it).first;
            int x = (*it).second;
            for(int i = 0 ; i < c ; i ++ ){
                res[mp(cur_x , cur_y)] = x;
                cur_x = cur_x + 1 ;
                if(cur_x == a){
                    cur_x = 0;
                    s ++ ;
                    cur_y = s;
                }
                else
                    cur_y = (cur_y + 1) % b;

            }
        }
        cout << ans << endl;
        cout << a << " " << b << endl;
        for(int i = 0 ; i < a ; i ++ ){
            for(int j = 0 ; j < b ; j ++ ){
                cout << res[mp(i , j)] << " ";
            }
            cout << endl;
        }
        return 0;
    }
    return 0;
}
