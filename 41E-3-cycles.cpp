///link to problem : https://codeforces.com/problemset/problem/41/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn=100+4;
const int inf=1e18;
const ll mod=1e9+7;
vector<pii> ans;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n; cin >> n;
    for(int i=1;i<=n/2;i++)
        for(int j=n/2+1;j<=n;j++)
            ans.pb(mp(i,j));
    cout << ans.size() << endl;
    for(int i=0;i<ans.size();i++)
        cout << ans[i].first << " " << ans[i].second << endl;
    return 0;
}
